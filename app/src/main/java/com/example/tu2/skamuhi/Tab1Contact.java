package com.example.tu2.skamuhi;


/**
 * Created by TU2 on 14/11/2017.
 */

import android.app.FragmentManager;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class Tab1Contact extends Fragment {

    //DEKLARASI CARDVIEW JURUSAN
CardView button1;
CardView button2;
CardView button3;
CardView button4;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab1contact, container, false);


        button1 = (CardView) rootView.findViewById(R.id.rpl);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), JurusanRpl.class);
                startActivity(intent);
            }
        });

        button2 = (CardView) rootView.findViewById(R.id.tkj);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), JurusanTkj.class);
                startActivity(intent);
            }
        });

        button3 = (CardView) rootView.findViewById(R.id.mm);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), JurusanMm.class);
                startActivity(intent);
            }
        });

        button4 = (CardView) rootView.findViewById(R.id.ak);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), JurusanAk.class);
                startActivity(intent);
            }
        });




        return rootView;



    }
}

