package com.example.tu2.skamuhi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.os.Handler;


    public class MainActivity extends AppCompatActivity {
        TextView tvSplash;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            tvSplash = (TextView) findViewById(R.id.tvSplash);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(getApplicationContext(), TabActivity.class));
                    finish();
                }
            }, 3000L);
        }
    }